import ballerina/io;
import ballerina/http;

type learner_profile record {|
  string username;
 string lastname;
  string firstname;
  string preferred_formats;
  string past_subjects;

|};

learner_profile[] profiles = [];

service /profiles on new http:Listener(8080){

    resource function post updateProfiles(@http:Payload learner_profile new_profile) returns json {
io:println("you are updating the learner profile");
profiles.push(new_profile);
return {done: "succesfully updated learner prfile" + new_profile.username + new_profile.lastname +new_profile.firstname + new_profile.preferred_formats + new_profile.past_subjects};
}


resource function get createProfiles()returns learner_profile[] {
    io:println("creating a learner profile");
    return profiles;

    
}

}

